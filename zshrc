autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' unstagedstr ' %F{196}●%f'
zstyle ':vcs_info:*' stagedstr ' %F{14}●%f'
zstyle ':vcs_info:git*' formats "%F{13}%s:(%b%u%c%F{13})%f"

precmd() {
    vcs_info
}

setopt prompt_subst

if [[ $EUID -ne 0 ]]; then
    PROMPT='%B%F{10}%n@%m%f %F{27}%~ ${vcs_info_msg_0_} %F{27}%#%f%b '
    #PROMPT='%B%F{10}%n@%m%f %F{27}%~ ${vcs_info_msg_0_} %F{27}%(!.#.>)%f%b '
    #RPROMPT='%B%F{51}%T%f'
else
    PROMPT='%B%F{9}%n@%m%f %F{27}%~ ${vcs_info_msg_0_} %F{27}%#%f%b '
    #PROMPT='%B%F{1}%n@%m%f %F{27}%~ ${vcs_info_msg_0_} %F{27}%(!.#.>)%f%b '
    #RPROMPT='%B%F{51}%T%f'
fi

ZLS_COLORS='di=34;1;42:ln=35:so=32:pi=33:ex=31:bd=46;34:cd=43;34:su=41;30:sg=46;30:tw=42;30:ow=43;30'
LSCOLORS='ECfxcxdxbxegedabagacad'

export LSCOLORS
export GPG_TTY=$(tty)
export PATH="$HOME/Library/Python/3.12/bin:$PATH"

zmodload zsh/complist
zstyle ':completion:*' list-colors ''

alias ls='ls -GoFht'
alias grep='egrep --color=auto'
